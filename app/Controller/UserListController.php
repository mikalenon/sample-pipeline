<?php

App::uses('Controller', 'Controller');

class UserListController extends Controller {
	
	public $uses = array(
		'User'
	);

	public function add(){
		$res = array();
		if ($this->request->data){
			$this->log($this->request->data, LOG_DEBUG);
			$res = $this->request->data;
			$this->User->save($res);
		}
		$this->set('res',$res);
	} 

	public function index(){
		$res = array();
		$allUser = $this->User->find('all', true);
		foreach($allUser as $obj){
			// $this->log($obj['User'], LOG_DEBUG);
			$res[] = $obj['User'];
		}
		$this->log($res, LOG_DEBUG);
		$this->set('res',$res);
	} 

	
}
