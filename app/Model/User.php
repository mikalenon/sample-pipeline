<?php
App::uses('AppModel', 'Model');

class User extends AppModel {

	public $useTable = 'user';
	public $useDbConfig = "default";
	const MODEL_NAME = 'User';
}
