
<div id="app" style="width:50%">
	<h1 style="font-size:20px"> ADD NEW USER  (<a href= "https://vbox.tokyo/cakephp/user_list/index">See list of Users</a>)</h1>
	<br><hr><hr><hr><br>
	
	
	<form id="myForm" method="post">

		Name <input type="text" name="name" v-model = "search.name">
		Year <input type="text" name="year" v-model = "search.year">
		Section <input type="text" name="section" v-model = "search.section">
		<br><br>
		<input type="button" v-on:click="submitFxn()" value="Add" style="background: #5bd1e7">
	</form>
	<div v-if=res_flg>
		<h1>確認画面</h1>
		<div v-for="(value, key) in res">
			<p>{{key}} is {{value}}</p>
		</div>
	</div>
</div>

<script>
	new Vue({
		el: '#app',
		data: {
			res: [],
			res_flg:0,
			search: {
				name: "",
				year: "",
				section: ""
			}
		},
		methods: {
			submitFxn() {
				let is_valid = this.checkForm();
				if(is_valid){
					document.getElementById("myForm").submit();
				}
			},
			checkForm() {
				for(var key in this.search) {
					if(this.search[key] == ""){
						return false;
					}
				}
				return true;
			}
		},
		mounted: function() {
			this.res=<?=json_encode($res)?>;
			if(Object.keys(this.res).length > 0){
				this.res_flg=1;
			}
		}
	});
</script>