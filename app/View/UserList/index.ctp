
<div id="app" style="width:50%">
	<h1 style="font-size:20px">DISPLAY LIST  (<a href= "https://vbox.tokyo/cakephp/user_list/add">Create User</a>)</h1>
	<br>
	<div v-if=res_flg>
		<table>
			<tr>
				<th>Name</th>
				<th>Year</th>
				<th>Section</th>
			</tr>
			<tr v-for="obj in res">
				<td>{{obj.name}}</td>
				<td>{{obj.year}}</td>
				<td>{{obj.section}}</td>
			</tr>
		</table>
	</div>
</div>

<script>
	new Vue({
		el: '#app',
		data: {
			res: [],
			res_flg:0,
		},
		methods: {
		},
		mounted: function() {
			this.res=<?=json_encode($res)?>;
			if(Object.keys(this.res).length > 0){
				this.res_flg=1;
			}
		}
	});
</script>