DROP TABLE user;
CREATE TABLE IF NOT EXISTS `test_db`.`user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` INT UNSIGNED NOT NULL,
  `year` INT UNSIGNED NOT NULL,
  `section` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`))
  ENGINE=InnoDB DEFAULT CHARSET=utf8
  ;

INSERT INTO `test_db`.`user` (`name`, `year`, `section`) VALUES
 ('Cindy', 2000, 'St. John'),
 ('Sheila', 1997, 'St. John'),
 ('Grace', 2002, 'St. John'),
 ('Mary', 1992, 'St. John'),
 ('Jose', 2000, 'St. John')
;

