SELECT id, employee_id, an_id
FROM m_kokyaku
WHERE employee_id = 637
-- result 713, INCEPTION's CLIENT

SELECT id
FROM m_kokyaku
WHERE an_id IN (SELECT an_id
FROM m_kokyaku
WHERE employee_id = 637);
-- result 17885, CLIENT WITH an_id == INCEPTION's CLIENT

SELECT first_keisai_end_ymd, nyukou_id, kokyaku_id, employee_id, jucyu_kingaku
FROM m_jucyu
WHERE YEAR(first_keisai_end_ymd) = '2018' AND kokyaku_id IN (SELECT id
FROM m_kokyaku
WHERE an_id IN (SELECT an_id
FROM m_kokyaku
WHERE employee_id = 637));

-- result 37115 total, 


SELECT first_keisai_end_ymd, nyukou_id, kokyaku_id, employee_id, SUM(jucyu_kingaku)
FROM m_jucyu
WHERE YEAR(first_keisai_end_ymd) = '2018' AND kokyaku_id IN (SELECT id
FROM m_kokyaku
WHERE an_id IN (SELECT an_id
FROM m_kokyaku
WHERE employee_id = 637))
GROUP BY kokyaku_id;
